<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    public function beranda ()
    {
        return view ('beranda');
    }
    public function show ()
    {
        $karyawan = DB::table('Karyawan')->get();

        return view ('data_karyawan.data',['k'=>$karyawan]);
    }
    public function tambah ()
    {
        return view ('data_karyawan.tambah');
    }

    public function prosesSimpan (Request $req)
    {
      
        DB::table('Karyawan')->insert(
            ['nama_karyawan'=>$req->nama,
             'no_karyawan'=>$req->no,
             'no_telp_karyawan'=>$req->notlp,
             'jabatan_karyawan'=>$req->jabatan,
             'devisi_karyawan'=>$req->divisi,
            ]);

       
        return redirect('/datas')->with('status', 'Berhasil Menambahkan Data!');
    }

    public function edit($id)
    {
        $datas = DB::table('Karyawan')->where('id', $id)->first();
      
        return view ('data_karyawan.edit', compact('datas'));
    }

    public function prosesEdit(Request $req, $id)
    {
        DB::table('Karyawan')->where('id', $id)
                    ->update([
                        'nama_karyawan'=>$req->nama,
                        'no_karyawan'=>$req->no,
                        'no_telp_karyawan'=>$req->notlp,
                        'jabatan_karyawan'=>$req->jabatan,
                        'devisi_karyawan'=>$req->divisi,
                    ]);
      
        return redirect('/datas')->with('status', 'Berhasil Mengupdate Data!');
    }

    public function delete($id)
    {
       DB::table('Karyawan')->where('id',$id )->delete();
       return redirect('/datas')->with('status', 'Berhasil Menghapus Data!');
    }
} 
